## Abstract
---

This is a simple implementation of the observation design pattern to implement a web page update notification service via text messages or emails.
There are times that one would like to know when a web page has been updated. For example
it would be nice to know when the assignment page for a course is changed. As mentioned above, users can be notified by updates via email, text and also can be printed out on the 
console. The main purpose of this application is to learn how observer patten can be implemented with reactive Java. Another aspect of this application is to handle dynamic changes in the 
system without changing the existing code. Factory pattern has been used to address this dynamic change.

    
## Usage
---

Step1 : Clone this repo, and do ``cd src/main/java/client``   
Step2 : Run ``WebPageUpdaterClient``

To update the notification source email, appropriate changes can be made to `/src/main/java/Notifications` in different email/phone/console class implementations. 