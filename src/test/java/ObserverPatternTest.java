import Client.WebPageUpdateNotifier;
import Notifications.*;
import URLParser.UrlParser;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import UrlUpdateNotificationService.UrlUpdateService;

import javax.mail.MessagingException;
import javax.mail.Transport;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ObserverPatternTest {


    String fileName;
    private WebPageUpdateNotifier notifier;

    @Before
    public void setUp() {

        fileName = System.getProperty("user.dir") + "/src/main/java/url.txt";
        notifier = new WebPageUpdateNotifier(fileName);
    }


    @Test
    public void testPageUpdationAlert() throws IOException, InterruptedException {

        HttpURLConnection mockHttp = mock(HttpURLConnection.class);
        when(mockHttp.getResponseCode()).thenReturn(200);

        when(mockHttp.getLastModified()).thenReturn(1000016302400L).thenReturn(2000016302400L);
        String spec = "http://www.google.com";

        URL url = createUrl(spec, mockHttp);
        UrlUpdateService urlService = new UrlUpdateService(url);
        INotify observer = mock(INotify.class);


        urlService.subscribeToWebPage(observer);

        String message = "Content of " + url.toString() + " has been updated";

        urlService.notifyContentUpdate();
        urlService.notifyContentUpdate();

        verify(observer, times(1)).onNext(message);
    }



    @Test
    public void testURLParsing() throws IOException {


        List<UrlUpdateService> expectedList = new ArrayList<>();

        UrlUpdateService url = new UrlUpdateService(new URL("http://www.eli.sdsu.edu/courses/fall18/cs635/notes/index.html"));
        expectedList.add(url);

        url = new UrlUpdateService(new URL("http://www.eli.sdsu.edu/index.html"));
        expectedList.add(url);

        UrlParser parser = new UrlParser();

        List<UrlUpdateService> actualList = parser.parse(new File(fileName));

        assertEquals(expectedList.size(), actualList.size());

        assertArrayEquals(expectedList.toArray(), actualList.toArray());
    }


    public static URL createUrl(String specifications, HttpURLConnection connection) {

        URL url = null;
        try {
            URL urlContext = new URL(specifications);
            URLStreamHandler urlStreamHandler = new URLStreamHandler() {
                @Override
                protected URLConnection openConnection(URL url) throws IOException {
                    return connection;
                }
            };

            url = new URL(urlContext, specifications, urlStreamHandler);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return url;
    }

    @Test
    public void testEmail() throws IOException, InterruptedException {

        INotify notifier = new EmailNotification(new String[]{"nnarwaria@sdsu.edu"});
        notifier.notify("This is a test email.");
    }

    @Test
    public void testEmailNotification() throws MessagingException {

        EmailNotification emailNotification = new EmailNotification(new String[]{"nnarwariae@sdsu.edu"});

        Transport transport = mock(Transport.class);
        emailNotification.setTransport(transport);

        emailNotification.notify("Test");

        verify(transport, times(1)).sendMessage(emailNotification.getEmailMessage(), emailNotification.getEmailMessage().getAllRecipients());
    }

    @Test
    public void testPhoneSMSNotification() throws MessagingException {

        PhoneNotification smsNotification = new PhoneNotification(new String[]{"5713381610", "lycamobile"});

        Transport transport = mock(Transport.class);
        smsNotification.setTransport(transport);

        smsNotification.notify("Test");

        verify(transport, times(1)).sendMessage(smsNotification.getPhoneMessage(), smsNotification.getPhoneMessage().getAllRecipients());
    }

    @Test
    public void testPhone() throws IOException, InterruptedException {

        INotify notifier = new PhoneNotification(new String[]{"5713381610", "lycamobile"});
        notifier.notify("This is a test SMS.");
    }


    @Test
    public void testClientNotifications() throws IOException{

        HttpURLConnection mockHttp = mock(HttpURLConnection.class);
        when(mockHttp.getResponseCode()).thenReturn(200);

        when(mockHttp.getLastModified()).thenReturn(6245118904499L);

        String spec = "http://www.google.com";

        URL url = createUrl(spec, mockHttp);
        UrlUpdateService urlService = new UrlUpdateService(url);

        INotify consoleObserver = UrlFactory.getObserver("console", null);
        urlService.subscribeToWebPage(consoleObserver);
        assertThat(consoleObserver, instanceOf(ConsoleNotification.class));

        String[] smsDetails =  {"5713381610", "lycamobile"};
        INotify phoneObserver = UrlFactory.getObserver("sms", smsDetails);
        urlService.subscribeToWebPage(phoneObserver);
        assertThat(phoneObserver, instanceOf(PhoneNotification.class));

        String[] emailDetails = new String[] {"nnarwaria@sdsu.edu"};
        INotify emailObserver = UrlFactory.getObserver("mail", emailDetails);
        urlService.subscribeToWebPage(emailObserver);
        assertThat(emailObserver, instanceOf(EmailNotification.class));

        urlService.notifyContentUpdate();
    }



}
