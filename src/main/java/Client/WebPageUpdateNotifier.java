package Client;

import UrlUpdateNotificationService.UrlUpdateService;

import URLParser.UrlParser;

import java.io.File;
import java.io.IOException;

import java.util.List;
public class WebPageUpdateNotifier {

    private String fileName;
    private static final long SLEEP_PERIOD = 10000;

    public WebPageUpdateNotifier(String fileName) {
        this.fileName = fileName;
    }

    /*
     * execute runs indefinitely, and keeps posting notifications to subscribed users in certain periods of time.
     */
    public void execute() throws IOException, InterruptedException {

        // parse url and arguments from file provided.
        File file = new File(fileName);
        UrlParser urlParser = new UrlParser();

        List<UrlUpdateService> updateServices = urlParser.parse(file);

        while(true){

            for (UrlUpdateService urlService: updateServices) {
                urlService.notifyContentUpdate();
            }
            Thread.sleep(SLEEP_PERIOD);
        }
    }

}
