package UrlUpdateNotificationService;


import Notifications.ConsoleNotification;
import Notifications.EmailNotification;
import Notifications.PhoneNotification;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import Notifications.INotify;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

public class UrlUpdateService {
    URL url;

    // 3 different notifications streams, one for each type of notification so that each type of notification
    // can be sent in parallel for same updates.
    Subject<String> updateAlerts;
    Subject<String> emailUpdates;
    Subject<String> phoneUpdates;


    long lastUpdated;

    public UrlUpdateService(URL url) {

        this.url = url;
        updateAlerts = PublishSubject.create();
        emailUpdates = PublishSubject.create();
        phoneUpdates = PublishSubject.create();

        lastUpdated = System.currentTimeMillis();
    }


    public void notifyContentUpdate() throws IOException{

        try{
            URLConnection connect = url.openConnection();
            long lastPageModification = connect.getLastModified();
            if (lastPageModification > lastUpdated) {
                updateAlerts.onNext("Content of " + url +" has been updated");
                phoneUpdates.onNext("Content of " + url +" has been updated");
                emailUpdates.onNext("Content of " + url +" has been updated");
                this.lastUpdated = lastPageModification;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }


    public URL getUrl() {
        return url;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) return false;

        return ((obj instanceof UrlUpdateService) && (((UrlUpdateService) obj).getUrl()).equals(this.url));
    }

    public void subscribeToWebPage(INotify observer) {

        if(observer instanceof PhoneNotification)
            phoneUpdates.subscribe(observer::onNext, Throwable::printStackTrace);
        else if(observer instanceof  EmailNotification)
            emailUpdates.subscribe(observer::onNext, Throwable::printStackTrace);
        else
            updateAlerts.subscribe(observer::onNext, Throwable::printStackTrace);
    }


    @Override
    public int hashCode() {
        return this.url.hashCode();
    }
}
