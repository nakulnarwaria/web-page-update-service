package URLParser;

import Notifications.INotify;
import Notifications.UrlFactory;
import UrlUpdateNotificationService.UrlUpdateService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.*;


// Extracts url and notification type along with necessary arguments for notification type from file provided.
public class UrlParser {

    HashMap<String, UrlUpdateService> urlUpdateServices;

    public UrlParser() {
        urlUpdateServices = new HashMap<>();
    }

    public List<UrlUpdateService> parse(File file) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        List<UrlUpdateService> urlServices = new LinkedList<>();

        String line;

        while ((line = bufferedReader.readLine()) != null) {

            line = line.trim();

            String[] commandOperands = line.split(" ");

            INotify observer = UrlFactory.getObserver(commandOperands[1], Arrays.asList(commandOperands).subList(2, commandOperands.length).toArray(new String[0]));

            if (urlUpdateServices.containsKey(commandOperands[0])) {

                urlUpdateServices.get(commandOperands[0]).subscribeToWebPage(observer);
            }
            else {
                UrlUpdateService urlService = new UrlUpdateService(new URL(commandOperands[0]));
                urlService.subscribeToWebPage(observer);
                urlServices.add(urlService);
                urlUpdateServices.put(commandOperands[0], urlService);
            }

        }

        return urlServices;
    }
}
