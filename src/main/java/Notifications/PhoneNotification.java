package Notifications;

import MailServerDetails.MailServer;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;

public class PhoneNotification extends INotify {


    private String phoneMMSDomain;

    private String fromEmail;

    private MimeMessage phoneMessage;
    private Transport transport;

    private static HashMap<String, String> carrierInfo = new HashMap<>();

    static {
        carrierInfo.put("alltel", "mms.alltelwireless.com");
        carrierInfo.put("att", "mms.att.net");
        carrierInfo.put("boost", "myboostmobile.com");
        carrierInfo.put("cricket", "mms.cricketwireless.net");
        carrierInfo.put("fi", "msg.fi.google.com");
        carrierInfo.put("uscellular", "mms.uscc.net");
        carrierInfo.put("verizon", "vzwpix.com");
        carrierInfo.put("sprint", "pm.sprint.com");
        carrierInfo.put("virgin","vmpix.com");
        carrierInfo.put("tmobile", "tmomail.net");
        carrierInfo.put("lycamobile", "mms.us.lycamobile.com");

    }

    public PhoneNotification(String[] args) {
        this.phoneMMSDomain = args[0] + "@" + getCellCarrierMMSDomain(args[1]);
        this.fromEmail = "test.lavaa@gmail.com";

        this.generateSMSMimeMessages();
        try{
            Transport transport = MailServer.getMailServerSession().getTransport("smtp");

            this.setTransport(transport);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }



    public MimeMessage getPhoneMessage() {
        return phoneMessage;
    }

    public void setPhoneMessage(MimeMessage phoneMessage) {
        this.phoneMessage = phoneMessage;
    }



    @Override
    public void notify(String message) {

        try {
            this.getPhoneMessage().setText(message);
            this.transport.connect();
            this.transport.sendMessage(this.getPhoneMessage(), this.getPhoneMessage().getAllRecipients());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void generateSMSMimeMessages(){
        try {
            // Create a default MimeMessage object.
            MimeMessage phoneMessage = new MimeMessage(MailServer.getMailServerSession());

            // Set contents in header
            phoneMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(phoneMMSDomain));
            phoneMessage.setFrom(new InternetAddress(fromEmail));

            // Set subject
            phoneMessage.setSubject("Content Update");

            this.setPhoneMessage(phoneMessage);

        } catch (MessagingException exception) {
            exception.printStackTrace();
        }
    }


    public static String getCellCarrierMMSDomain(String carrierName) {
        return carrierInfo.get(carrierName);
    }
}
