package Notifications;

import io.reactivex.disposables.Disposable;


public class ConsoleNotification extends INotify {


    @Override
    public void notify(String message) {
        System.out.println(message);
    }


}
