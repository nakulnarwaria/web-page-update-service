package Notifications;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


// to be implemented by notification types
public abstract class INotify implements Observer{

    abstract public void notify(String message);

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onNext(Object o) {
        if (!(o instanceof String))
            return;
        this.notify((String) o);
    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onComplete() {

    }
}
