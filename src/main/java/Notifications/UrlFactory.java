package Notifications;


// Factory for Notification objects
public class UrlFactory {


    public static INotify getObserver(String notifcationmethod, String[] args) {

        switch (notifcationmethod) {
            case "sms":
                return new PhoneNotification(args);
            case "mail":
                return new EmailNotification(args);
            case "console":
                return new ConsoleNotification();
        }

        return null;
    }


}
