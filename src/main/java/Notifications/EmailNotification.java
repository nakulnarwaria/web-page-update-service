package Notifications;


import MailServerDetails.MailServer;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailNotification extends INotify {

    private String sendTo;
    private String fromEmail;

    private Transport transport;
    private MimeMessage emailMessage;

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }


    public MimeMessage getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(MimeMessage emailMessage) {
        this.emailMessage = emailMessage;
    }


    public EmailNotification(String[] args) {
        this.sendTo = args[0];
        this.fromEmail = "nakulnarwaria17@gmail.com";

        this.generateEmailMimeMessage();
        try{
            Transport transport = MailServer.getMailServerSession().getTransport("smtp");

            this.setTransport(transport);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public void notify(String message) {

        try {
            this.getEmailMessage().setText(message);
            this.transport.connect();
            this.transport.sendMessage(this.getEmailMessage(), this.getEmailMessage().getAllRecipients());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void generateEmailMimeMessage(){
        try {
            // Create a default MimeMessage object.
            MimeMessage emailMessage = new MimeMessage(MailServer.getMailServerSession());

            // Set contents in header
            emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(sendTo));
            emailMessage.setFrom(new InternetAddress(fromEmail));

            // Set subject
            emailMessage.setSubject("Content Update");

            this.setEmailMessage(emailMessage);
        } catch (MessagingException exception) {
            exception.printStackTrace();
        }
    }


}
